Spree::BaseHelper.class_eval do

  def display_pre_order(product_or_variant)
    Spree::Money.new(@product.master.price_in(@product.master.currency).try(:pre_order_amount), { currency: @product.master.currency})
  end
end