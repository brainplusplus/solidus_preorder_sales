Deface::Override.new(:virtual_path => 'spree/admin/products/_form',
                     :name => 'add_is_pre_order_to_product_form',
                     :insert_bottom  => "[data-hook='admin_product_form_additional_fields']",
                     :text => "
    <%= f.field_container :is_pre_order do %>
          <%= f.label :is_pre_order do %>
            <%= f.check_box :is_pre_order %> <%= Spree::Product.human_attribute_name(:is_pre_order) %>
          <% end %>
      <%= f.error_message_on :is_pre_order %>
    <% end %>
  ")