Deface::Override.new(:virtual_path => 'spree/admin/prices/_form',
                     :name => 'add_pre_order_amount_to_price_form',
                     :insert_after => "erb[loud]:contains('text_field :price')",
                     :text => "
    <%= f.field_container :pre_order_amount do %>
          <%= f.label :pre_order_amount %>
          <%= f.text_field :pre_order_amount, value: f.object.pre_order_price, class: 'fullwidth title' %>
        <% end %>
  ")