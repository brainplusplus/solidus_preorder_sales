Deface::Override.new(:virtual_path => 'spree/products/_cart_form',
                     :name => 'add_pre_order_amount_to_cart_form',
                     :insert_after => "#product-price div",
                     :text => "
                     <% if @product.is_pre_order? %>
    <h6 class='product-section-title-pre-order'>PRICE PRE ORDER</h6>
          <div>
            <span class='price selling preorder' itemprop='preorder'>
                <%= display_pre_order(@product) %>
            </span>
            <span itemprop='priceCurrencyPreOrder' content='<%= current_pricing_options.currency %>''></span>
          </div>
      <% end %>
  ")