class AddPreOrderAmountToSpreePrices < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_prices, :pre_order_amount, :decimal
  end
end
