class AddIsPreOrderToSpreeProducts < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_products, :is_pre_order, :boolean
  end
end
